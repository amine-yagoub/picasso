import { Injectable } from '@nestjs/common'

@Injectable()
class AppService {
  getHello () {
    return JSON.stringify('Hello git Picasso App Academy!')
  }
}
export default AppService
