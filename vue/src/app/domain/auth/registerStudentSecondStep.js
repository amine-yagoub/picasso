import { concatMap, map, pluck, tap, throttleTime } from "rxjs/operators";
import { hidePageLoading, hideSpinner, showPageLoading, showSpinner } from "src/app/services/operators";
import student from "src/app/services/student";
import employee from "src/app/services/employee";
import { iif, of } from "rxjs";
import course from "src/app/services/course";

export default {
  name: 'registerStudentSecondStep',
  domStreams: ['onSubmitClient$'],
  subscriptions () {
    const { getters } = this.$store
    return {
      submit$: this.onSubmitForm(),
      pageLoading$: getters['app/getPageLoading$'],
      courses$: this.populateData(getters['course/getCourses$'], course.getCourses$()),
      employees$: this.populateData(getters['employee/getEmployees$'], employee.getEmployees$())
    }
  },
  model: {
    prop: 'student$'
  },
  props: {
    loading$: {
      type: Boolean,
      required: true
    },
    student$: {
      type: Object,
      required: true
    }
  },
  data () {
    return {
      course: null,
      registerFees: null,
      proceduresDate: null,
      employeeInCharge: null,
      courseDetails: false
    }
  },
  methods: {
    populateData(source$, stream$) {
      return source$.pipe(
        showPageLoading(),
        concatMap(data => iif(() => data.length === 0, stream$, of(data))),
        hidePageLoading(),
        tap(result => {
          if(!Array.isArray(result)) {
            this.$store.getters["app/getNotify$"].next('getDataFail')
            this.getStep$.next(1)
          }
        })
      )
    },
    onSubmitForm () {
      return this.onSubmitClient$.pipe(
        showSpinner(),
        throttleTime(1500),
        pluck('data'),
        concatMap(student.signUpClient$),
        map(this.handleErrors),
        hideSpinner(),
        tap(result => this.handleResult(result))
      )
    },
    handleErrors (response) {
      if (response.statusCode === 201) {
        return true
      }
      if (response.statusCode === 422) {
        const { message } = response
        const phone = this.getExternalErrors('phone', message)
        if(phone.message === 1) {
          this.getNotify$.next('accountPhoneExist')
        }
        const name = this.getExternalErrors('name', message)
        if(name.message === 1) {
          this.getNotify$.next('accountNameExist')
        }
        this.getStep$.next(1)
      }
      return false
    },
    handleResult(result) {
      if(result) {
        const { getters } = this.$store
        getters["student/getStudent$"].next({})
        getters['app/getRoute$'].next({name: 'employeeManageStudents'})
        this.getNotify$.next('accountSuccess')
        this.getStep$.next(1)
      }
    },
    getExternalErrors (field, errors) {
      let result = {
        valid: true,
        message: null
      }
      if (Object.keys(errors).includes(field)) {
        result = {
          valid: false,
          message: errors[field]
        }
      }
      return result
    }
  },
  computed: {
    submitData () {
      return {
        course: {
          status: 'pending',
          numberOfPaidInstallments: 0,
          currentUnPaidInstallments: 0,
          ...this.course
        },
        ...this.student$
      }
    }
  },
  watch: {
    'course': function(val) {
      this.courseDetails = !!val
    }
  }
}
