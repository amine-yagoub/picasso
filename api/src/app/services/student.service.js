import { BadRequestException, Dependencies, Injectable } from "@nestjs/common";
import AuthHelperService from "./authHelper.service";
import { getModelToken } from "@nestjs/mongoose";
import { ConfigService } from "@nestjs/config";

@Dependencies(getModelToken('students'),getModelToken('auth'), ConfigService)
@Injectable()
class StudentService extends AuthHelperService{
  #allowedQueries = ['limit', 'role']
  /**
   * AuthService Constructor
   *
   * @param studentModel
   * @param authModel
   * @param configService
   */
  constructor (studentModel, authModel, configService) {
    super(configService)
    this.studentModel = studentModel
    this.authModel = authModel
  }
  
  /**
   *
   * Sign Up the Client.
   *
   * @param body
   * @returns {Promise<any>}
   */
  async tryToSignUpTheClient (body) {
    body.code = Number(Date.now().toString().slice(5))
    body.role = 'client'
    body.courses = [{
      status: 'pending',
      joinDate: Date.now(),
      ...body.course
    }]
    return await new this.studentModel(body).save()
  }
  
  /**
   *
   * Update the Client th new student.
   *
   * @param body
   * @returns {Promise<any>}
   */
  async updateClientToNewStudent (body) {
    const { phone } = body
    const pwd = this.generatePassword()
    body.role = 'newStudent'
    body.picture = '~assets/avatar.png'
    body.courses = [{
      status: 'active',
      numberOfPaidInstallments: 1,
      currentUnPaidInstallments: 0,
      ...body.course
    }]
    body.sections = [{
      status: 'active',
      joinDate: Date.now(),
      ...body.section
    }]
    const student = await this.studentModel.findOneAndUpdate({ phone }, body, {new: true, useFindAndModify: false})
    await this.authModel({
      phone,
      password: await this.pwdHash(pwd),
      model: student._id,
      onModel: 'students'
    }).save()
    return { student, pwd }
  }
  
  /**
   *
   * @param code
   */
  getStudentByCode(code) {
    return this.studentModel.findOne({ code: code })
  }
  
  /**
   *
   * @param queries
   * @returns {Promise<*>}
   */
  async paginate(queries) {
    for (const queryKey of Object.keys(queries)) {
      if(!this.#allowedQueries.includes(queryKey)) {
        throw new BadRequestException()
      }
    }
    const { role, limit } = queries
    console.log(queries, limit)
    const query = { }
    const options = {
      // select:   'title date author',
      // sort:     { date: -1 },
      populate: 'employeeInCharge',
      lean:     true,
      // offset:   20,
      limit: 10
    };
    switch (true) {
      case !!role:
        query.role = role
      case !!limit:
        options.limit = limit
      default:
        break
    }
    return await this.studentModel.paginate(query, options)
  }
  
  /**
   * Group on role, then count how many students in each role
   *
   * @returns {Promise<*>}
   */
  async countStudentByRole() {
    const aggregatorOpts =  [
      {
        $group: {
          _id: {
            role: '$role'
          },
          count: {
            $sum: 1
          }
        }
      }
    ]
    return await this.studentModel.aggregate(aggregatorOpts).exec()
  }
}
export default StudentService
