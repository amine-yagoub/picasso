const getters = {
  getPagination$: state => state.pagination$,
  getEmployees$: state => state.employees$,
  getEmployee$: state => state.employee$,
  getRequestPagination$: state => state.requestPagination$,
}
export default Object.freeze(getters)
