const getters = {
  getSignUpStep$: state => state.signUpStep$,
  getPagination$: state => state.pagination$,
  getStudents$: state => state.students$,
  getChangeCourse$: state => state.isChangeCourse$,
  getStudent$: state => state.student$,
  getRequestPagination$: state => state.requestPagination$,
}
export default Object.freeze(getters)
