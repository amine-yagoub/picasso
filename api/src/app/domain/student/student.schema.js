import { Schema } from "mongoose";
import mongoosePaginate from "mongoose-paginate-v2"
const StudentSchema = new Schema({
  code: {
    type: Number,
    required: true,
    index: { unique: true }
  },
  name: {
    type: String,
    required: true,
    index: { unique: true }
  },
  phone: {
    type: String,
    required: true,
    index: { unique: true }
  },
  role: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  qualification: {
    type: String,
    required: true
  },
  recordNumber: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    default: Date.now()
  },
  idNumber: String,
  cardId: String,
  picture: String,
  phone2: String,
  notes: Array,
  absents: Array,
  proceduresDate: {
    type: Date,
    required: true
  },
  courses: {
    type: Array,
    required: true
  },
  sections: Array,
  registerFees: {
    type: Number,
    required: true
  },
  // employeeInCharge: {
  //   type: Object,
  //   required: true
  // }
  employeeInCharge: {
    type: Schema.Types.ObjectId,
    ref: "employees",
    required: true
  }
});
StudentSchema.plugin(mongoosePaginate);
export default StudentSchema
