import { financial } from "src/app/domain/shared/utils.shared";
import locals from "../shared/locals";

export default {
  name: "registerStudentHelper",
  computed: {
    locals () {
      return locals
    },
    getStep$() {
      return this.$store.getters['student/getSignUpStep$']
    },
    getNotify$() {
      return this.$store.getters["app/getNotify$"]
    }
  },
  methods: {
    getCoursePrice (course) {
      return financial(course.price)
    },
    computePrice (course) {
      const { price, discountPercentage } = course;
      return financial(price - (price * (discountPercentage / 100)))
    },
    computeInstallmentPrice (course) {
      if (course.numberOfInstallments === 0) {
        return this.computePrice(course)
      }
      return financial((this.computePrice(course)) / course.numberOfInstallments)
    }
  }
};
