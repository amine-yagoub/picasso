import app from 'src/app/services/app'

const getters = {
  getNotify$: state => state.notify$,
  getConfig: state => state.config ? state.config.data : {},
  getLoading$: state => state.loading$,
  getPageLoading$: state => state.pageLoading$,
  getRoute$: state => state.route$,
  getNow: state => state.time,
  getTime: state => value => app.timeBetweenString(Math.floor(state.time / 1000), value),
  getAge: state => value => app.timeBetweenObject(Math.floor(state.time / 1000), value),
  getBroadcastChannel: state => state.broadcastChannel
}
export default Object.freeze(getters)
