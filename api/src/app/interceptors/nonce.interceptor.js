
import { Injectable, Dependencies } from '@nestjs/common'
import { map } from 'rxjs/operators'
import { ConfigService } from '@nestjs/config'

@Injectable()
@Dependencies(ConfigService)
class NonceInterceptor {
  /**
   *
   * @param configService
   */
  constructor (configService) {
    this.configService = configService
  }

  /**
   * Intercept response
   *
   * @param context
   * @param next
   * @returns {Observable<*>}
   */
  intercept (context, next) {
    return next
      .handle()
      .pipe(
        map(({ jwt, data, nonce }) => {
          const response = context.switchToHttp().getResponse()
          response.res.setHeader('Set-Cookie', this.buildCookie(nonce))
          return { jwt, data }
        })
      )
  }

  /**
   * Build Http Cookie.
   *
   * @param value
   * @returns {string}
   */
  buildCookie (value) {
    let secure = ''
    let name = this.configService.get('jwt.nonceName').toLowerCase()
    const age = this.configService.get('jwt.nonceAge')
    if (this.configService.get('api.protocol') === 'https') {
      name = `__Host-${name.charAt(0).toUpperCase()}${name.slice(1)}`
      secure = '; Secure'
    }
    return `${name}=${value}; path=/; Max-Age=${age}; SameSite=Strict; HttpOnly${secure}`
  }
}
export default NonceInterceptor
