import { Schema } from "mongoose";

export const CourseSchema = new Schema({
  name: {
    type: String,
    required: true,
    index: { unique: true }
  },
  price: {
    type: Number,
    required: true
  },
  discountPercentage: Number,
  numberOfInstallments: Number,
  created: {
    type: Date,
    default: Date.now()
  }
});
