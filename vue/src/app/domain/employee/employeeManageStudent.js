import employeeStudentsData, { clients, common, students } from "./employeeStudentsData";
import { concatMap, map, tap, withLatestFrom } from "rxjs/operators";
import student from "src/app/services/student";
import { hideSpinner, showSpinner } from "src/app/services/operators";
import { init } from "src/app/services/pagination";
import app from "src/app/services/app";

export default {
  name: "employeeManageStudent",
  meta() {
    return {
      title: app.titleTemplate(this.$t('headers.manageStudents')),
      // meta tags
      meta: {
        name: { name: "robots", content: "noindex,nofollow" }
      }
    };
  },
  extends: employeeStudentsData,
  domStreams: ["deleteStudent$", "archiveStudent$"],
  subscriptions () {
    const { getters } = this.$store;
    return {
      loading$: getters["app/getLoading$"],
      pagination$: getters["student/getPagination$"],
      students$: getters["student/getStudents$"],
      paginate$: this.paginateProducts$()
    };
  },
  methods: {
    onRequest ({ pagination, filter }) {
      return this.getRequestPagination$.next({
        request: pagination,
        current: this.pagination$,
        filter
      });
    },
    paginateProducts$() {
      return this.getRequestPagination$.pipe(
        showSpinner(),
        map(pagination => {
          const { request, filter } = pagination;
          // if (request.page > current.page) {
          //   request.isNext = true;
          //   request.isPrev = false;
          // } else if (request.page < current.page) {
          //   request.isNext = false;
          //   request.isPrev = true;
          // }
          request.filter = filter;
          this.getPagination$.next(request);
          return request;
        }),
        concatMap(options => student.paginateStudents$(options)),
        withLatestFrom(this.getPagination$),
        hideSpinner(),
        tap(([response, pagination]) => {
          console.log(response)
          if (Array.isArray(response.docs)) {
            const { docs, ...meta } = response
            this.getStudents$.next(docs);
            // pagination.rowsNumber = response.totalDocs;
            // pagination.links = response.links;
            this.getPagination$.next(meta);
          }
        })
      )
    },
    getTime (time) {
      return this.$store.getters["app/getTime"](time);
    },
    deleteStudents () {},
    toggleArchiveOfStudents (val) {},
    onStudentsListClick (val) {
      const { label, role } = val;
      this.currentList = label;
      this.visibleColumns = ["client", "clientArchive", "clientVisit"].includes(role)
        ? [...common, ...clients]
        : [...common, ...students];
      init.request.role = role
      this.getRequestPagination$.next(init)
    }
  },
  computed: {
    getRequestPagination$ () {
      return this.$store.getters["student/getRequestPagination$"]
    },
    getPagination$ () {
      return this.$store.getters["student/getPagination$"]
    },
    getStudents$ () {
      return this.$store.getters["student/getStudents$"]
    }
  }
};
