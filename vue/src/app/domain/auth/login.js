import { concatMap, map, pluck, tap, throttleTime } from "rxjs/operators";
import app from "src/app/services/app";
import { hideSpinner, showSpinner } from "src/app/services/operators";
import auth from "src/app/services/auth";

export default {
  name: "login",
  meta() {
    return {
      title: app.titleTemplate(this.$t('buttons.signIn')),
      // meta tags
      meta: {
        name: { name: "robots", content: "noindex,nofollow" }
      }
    };
  },
  domStreams: ['signIn$'],
  subscriptions() {
    return {
      loading$: this.$store.getters["app/getLoading$"],
      isPhoneValid$: this.validatePhone(),
      submit$: this.onSubmitForm()
    };
  },
  data() {
    return {
      phone: null,
      password: null,
      isPasswordValid: null,
      rememberMe: false,
    }
  },
  methods: {
    validatePhone () {
      return this.$watchAsObservable('phone').pipe(
        pluck('newValue'),
        map(val => !!val && val.length === 12 ? null : this.$t('errors.phone')))
    },
    toggleRememberMe(event) {
      event.target.focus();
    },
    onSubmitForm() {
      return this.signIn$.pipe(
        showSpinner(),
        throttleTime(1000),
        pluck('data'),
        concatMap(auth.signIn$),
        hideSpinner(),
        tap(result => this.handleResult(result))
      )
    },
    handleResult(result) {
      const {jwt, data} = result
      if(jwt && data) {
        auth.setJwt(this.rememberMe, jwt)
        this.getUser$.next(data)
        if(['employee'].includes(data.role)) {
          return this.getRoute$.next('employeeDashboard')
        }
        return this.getRoute$.next('me')
      }
      if(result.statusCode === 401) {
        this.isPasswordValid = this.isPhoneValid$ = this.$t('errors.credentialsIncorrect')
        return false
      }
      this.$store.getters["app/getNotify$"].next("generalError")
    },
    resetInput() {
      this.isPasswordValid = this.isPhoneValid$ = null
    }
  },
  computed: {
    getCsrfToken() {
      return this.$store.getters["auth/getCsrfToken"]
    },
    getRoute$() {
      return this.$store.getters["app/getRoute$"]
    },
    getUser$() {
      return this.$store.getters["auth/getUser$"]
    },
    loginData() {
      return {
        phone: this.phone,
        password: this.password
      }
    },
    isSubmitDisabled() {
      return this.loading$ || !!this.isPasswordValid || !!this.isPhoneValid$
    }
  }
};
