import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { EmployeeSchema } from "./employee.schema";
import EmployeeService from "../../services/employee.service";
import EmployeeController from "../../controllers/employee.controller";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'employees', schema: EmployeeSchema }]),
  ],
  controllers: [EmployeeController],
  providers: [EmployeeService]
})
class EmployeeModule {}
export default EmployeeModule
