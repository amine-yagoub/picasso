import {
  Controller,
  Dependencies,
  Get,
  Bind,
  Param,
  Logger
} from "@nestjs/common";

import EmployeeService from "../services/employee.service";

@Controller("employees")
@Dependencies(EmployeeService)
class EmployeeController {
  /**
   *
   * @param employeeService
   */
  constructor (employeeService) {
    this.employeeService = employeeService;
  }
  
  /**
   * Get all employees.
   *
   * @returns {Promise<*>}
   */
  @Get()
  async getAll () {
    try {
      return await this.employeeService.findAll();
    } catch (error) {
      Logger.error(error);
    }
  }
  
  /**
   * Get all courses.
   *
   * @returns {Promise<*>}
   */
  @Get(":id/sections")
  @Bind(Param("id"))
  async getAllSections (id) {
    try {
      return await this.employeeService.findCourseSections(id);
    } catch (error) {
      Logger.error(error);
    }
  }
}

export default EmployeeController;
