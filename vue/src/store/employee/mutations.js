const mutations = {
  setPagination$: (state, payload$) => (state.pagination$ = payload$),
  setEmployees$: (state, payload$) => (state.employees$ = payload$),
  setEmployee$: (state, payload$) => (state.employee$ = payload$),
  setRequestPagination$: (state, payload$) => (state.requestPagination$ = payload$),
}

export default Object.freeze(mutations)
