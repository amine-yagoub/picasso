const getters = {
  getPagination$: state => state.pagination$,
  getCourses$: state => state.courses$,
  getSections$: state => state.sections$,
  getCourse$: state => state.course$,
  getRequestPagination$: state => state.requestPagination$,
}
export default Object.freeze(getters)
