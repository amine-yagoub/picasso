import api from "src/app/services/api";
import { iif, of } from "rxjs";
import { concatMap, map, concat, tap, delay } from "rxjs/operators";
import auth from "src/app/services/auth";
import { hidePageLoading, showPageLoading } from "src/app/services/operators";

export const publicRoutes = ["register", "login", "forgot"];
export const csrfRoutes = ["login", "me"];
export const errorPages = {
  901: "cowboy",
  404: "notFound",
  403: "forbidden",
  400: "badRequest",
  303: "seeOther"
};
const weHaveUser = data => !!data._id;
const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
export const helper = {
  auth: false,
  user: {
    role: null,
    emailConfirmed: false
  },
  csrf: null,
  fetched: false,
  fetchUser$(user$) {
    return user$.pipe(
      showPageLoading(),
      concatMap(user =>
        {
          return iif(
            () => (Object.entries(user).length === 0 && api.getJwt()), auth.userInfo$(), of(user)
          )
        }
      ),
      hidePageLoading()
    );
  },
  fetchCsrf: async stream$ => {
    stream$.subscribe(response => {
      helper.fetched = true;
      helper.csrf = response.data ? response.data.token : null;
    });
    return await helper.waitUntilFetch();
  },
  waitUntilFetch: async () => {
    helper.fetched = false;
    for (let i = 0; i < 5; i++) {
      if (helper.fetched) {
        helper.fetched = false;
        break;
      }
      await sleep(200);
    }
  }
};
