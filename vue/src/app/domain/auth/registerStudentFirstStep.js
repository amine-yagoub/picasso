import { map, pluck, tap } from "rxjs/operators";
import validator from 'src/app/services/validator'

export default {
  name: 'registerStudentFirstStep',
  domStreams: ['signUp$'],
  subscriptions () {
    return {
      isNameValid$: this.validateName(),
      isPhoneValid$: this.validatePhone(),
      submitForm$: this.onSubmitForm()
    }
  },
  model: {
    prop: 'student$'
  },
  props: {
    loading$: {
      type: Boolean,
      required: true
    },
    student$: {
      type: Object,
      required: true
    }
  },
  data () {
    return {
      options: [
        this.$t('labels.highSchool'),
        this.$t('labels.bachelor'),
        this.$t('labels.master'),
        this.$t('labels.doctoral')
      ]
    }
  },
  methods: {
    validateName () {
      return this.$watchAsObservable('student$.name').pipe(
        pluck('newValue'),
        map(val => this.checkName('fullName', val)))
    },
    validatePhone () {
      return this.$watchAsObservable('student$.phone').pipe(
        pluck('newValue'),
        map(val => !!val && val.length === 12 ? null : this.$t('errors.phone')))
    },
    checkName (label, value) {
      const min = 3
      const max = 50
      return !validator.between(value, min, max)
        ? this.$t('errors.length', {
          field: this.$t(`labels.${label}`),
          min,
          max
        })
        : !validator.alphaNum(value)
          ? this.$t('errors.fieldChar', { field: this.$t(`labels.${label}`) })
          : null
    },
    onSubmitForm () {
      const { getters } = this.$store
      return this.signUp$.pipe(
        pluck('data'),
        tap(data => {
          getters['student/getStudent$'].next(data)
          getters['student/getSignUpStep$'].next(2)
        })
      )
    }
  },
  computed: {
    submitData() {
      return this.student$
    }
  }
}
