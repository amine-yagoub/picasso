import Api from './api'
import notify from './notify'
import { i18n } from 'src/boot/i18n'
const CONFIG_ROUTE = `${Api.getBaseUri()}/config`
const SITE_TITLE = 'أكاديمية بيكاسو'

export default Object.freeze({
  prefetchGeneralConfig (headers = {}) {
    return Api.prefetch(CONFIG_ROUTE, headers)
  },
  notify (name, options = {}) {
    const call = notify[name]
    return typeof call === 'function' ? call(options) : null
  },
  getLabels () {
    return {
      timeNow: i18n.t('labels.timeNow'),
      timeMonth: i18n.t('labels.timeMonth'),
      timeDay: i18n.t('labels.timeDay'),
      timeHour: i18n.t('labels.timeHour'),
      timeMinute: i18n.t('labels.timeMinute'),
      since: i18n.t('labels.since')
    }
  },
  timeBetweenObject (now, value) {
    const labels = this.getLabels()
    let seconds = now - value
    const def = {
      time: '',
      label: labels.timeNow
    }
    if (seconds > 0) {
      let minutes = Math.floor(seconds / 60)
      let hours = Math.floor(minutes / 60)
      let days = Math.floor(hours / 24)
      const months = Math.floor(days / 30)
      days %= 30
      hours %= 24
      minutes %= 60
      seconds %= 60
      return months !== 0
        ? {
          time: months,
          label: labels.timeMonth
        }
        : days !== 0
          ? {
            time: days,
            label: labels.timeDay
          }
          : hours !== 0
            ? {
              time: hours,
              label: labels.timeHour
            }
            : minutes !== 0
              ? {
                time: minutes,
                label: labels.timeMinute
              }
              : def
    }
    return def
  },
  timeBetweenString (now, value) {
    const labels = this.getLabels()
    const def = this.timeBetweenObject(now, value)
    return `${labels.since} ${def.time} ${def.label}`
  },
  formatPrice (price) {
    return Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: i18n.t('labels.currency')
    }).format(price)
  },
  formatDate(date) {
    return (new Date(date)).toLocaleDateString('ar-DZ', { weekday: 'long', year: 'numeric', month: 'numeric', day: 'numeric' })
  },
  titleTemplate: title => (title ? `${SITE_TITLE} - ${title}` : SITE_TITLE)
})
