import {
  Controller,
  Dependencies,
  Post,
  Get,
  Body,
  Bind,
  Param,
  Request,
  UseGuards,
  UseInterceptors,
  UnauthorizedException, Logger, BadRequestException, Delete, Put, Query
} from "@nestjs/common";

import { UnprocessableEntityException } from '../exceptions/UnprocessableEntityException'
import StudentService from "../services/student.service";

@Controller('students')
@Dependencies(StudentService)
class StudentController {
  /**
   *
   * @param studentService
   */
  constructor (studentService) {
    this.studentService = studentService
  }

  /**
   * SignUp the user.
   *
   * @param body
   * @returns {Promise<*>}
   */
  @Post('register')
  @Bind(Body())
  async signUpClient (body) {
    try {
      await this.studentService.tryToSignUpTheClient(body)
      return { message: 'Created', statusCode: 201 }
    } catch (error) {
      Logger.error(error)
      if (error.code === 11000) {
        throw new UnprocessableEntityException(error.keyPattern)
      }
      throw new BadRequestException()
    }
  }
  
  /**
   * SignUp the user.
   *
   * @param body
   * @returns {Promise<*>}
   */
  @Put('register')
  @Bind(Body())
  async updateClientToNewStudent (body) {
    try {
      return await this.studentService.updateClientToNewStudent(body)
    } catch (error) {
      Logger.error(error)
    }
  }
  
  /**
   * Group on role, then count how many students in each role
   *
   * @returns {Promise<*>}
   */
  @Get('group-role')
  async groupAndCount() {
    return await this.studentService.countStudentByRole()
  }
  
  
  /**
   * Group on role, then count how many students in each role
   *
   * @returns {Promise<*>}
   */
  @Get()
  @Bind(Query())
  async getStudents(queries) {
    return await this.studentService.paginate(queries)
  }
  
  /**
   * Get Student by code number.
   *
   * @returns {Promise<*>}
   */
  @Get(":code")
  @Bind(Param("code"))
  async getStudent(code) {
    try {
      return await this.studentService.getStudentByCode(code)
    } catch (error) {
      Logger.error(error)
    }
  }
  
}
export default StudentController
