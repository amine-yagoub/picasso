export default Object.freeze({
  required (val) {
    return !!val && !!String(val).trim().length
  },
  alphaNum (val) {
    // eslint-disable-next-line
    return /^[٠١٢٣٤٥٦٧٨٩0-9A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ\s_-ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيًٌٍَُِّْٰ]*$/i.test(val)
  },
  between (val, min, max) {
    let value = Number(val)
    if (typeof val === 'string') {
      value = val.length
    }
    return Number(min) <= value && Number(max) >= value
  },
  numeric (val) {
    return /^[0-9]+$/.test(val) || /^[٠١٢٣٤٥٦٧٨٩]+$/.test(val)
  },
  email (val) {
    // eslint-disable-next-line
    const e = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return e.test(String(val))
  },
  password (val) {
    const p = /^.*(?=.{8,})((?=.*[!§@/?°£µ~#$%^&*()"'\-_=+{};:,<.>]))(?=.*\d)((?=.*[a-z]))((?=.*[A-Z])).*$/
    return p.test(val)
  },
  oneOf (val, options = []) {
    return options.some(item => {
      return item === val
    })
  }
})
