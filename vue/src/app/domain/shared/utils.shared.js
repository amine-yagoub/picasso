/* eslint-disable */
import LoadingComponent from 'components/partials/LoadingComponent'
import ErrorComponent from 'components/partials/ErrorComponent'

export default function asyncComponent(name) {
  return () => ({
    component: import(`src/${name}.vue`),
    loading: LoadingComponent,
    error: ErrorComponent,
    delay: 200,
    timeout: 3000
  })
}

export function financial(x) {
  return Number.parseFloat(x).toFixed(2)
}
