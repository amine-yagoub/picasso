import {
  Dependencies,
  Injectable,
  UnauthorizedException
} from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import AuthService from '../services/auth.service'

@Injectable()
@Dependencies(AuthService)
class JwtAuthGuard extends AuthGuard('jwt') {
  constructor (authService) {
    super()
    this.authService = authService
  }

  canActivate (context) {
    this.authService.request = context.switchToHttp().getRequest()
    return super.canActivate(context)
  }

  handleRequest (err, user, info) {
    // You can throw an exception based on either "info" or "err" arguments
    if (err || !user) {
      console.log(err)
      throw err || new UnauthorizedException()
    }
    return user
  }
}
export default JwtAuthGuard
