import { registerAs } from '@nestjs/config'

export default registerAs('database', () => {
  const host = process.env.DATABASE_HOST
  const port = process.env.DATABASE_PORT
  const name = process.env.DATABASE_NAME
  const user = process.env.DATABASE_USER
  const pwd = process.env.DATABASE_PASSWORD
  return {
    uri: `mongodb://${user}:${pwd}@${host}:${port}/${name}`,
    redis: {
      host: 'redis',
      port: '6379',
      password: 'password123'
    }
  }
})
