import { LocalStorage, SessionStorage, LoadingBar } from 'quasar'
import { Observable } from 'rxjs'
import http from 'http'
export const TOKEN = 'token'
const api = {
  getBaseUri () {
    return `${process.env.API}:${process.env.API_PORT}`
  },
  handleResponse (response) {
    if ([204, 205, 304].includes(response.status)) {
      return Promise.resolve(null)
    }
    return response.json().then(json => {
      if (!response.ok) {
        if (response.status === 400) {
          this.removeJwt()
        }
        return Promise.reject(json)
      }
      return json
    })
  },
  getJwt () {
    let jwt = null
    if (process.env.CLIENT) {
      if (LocalStorage.has(TOKEN)) {
        jwt = LocalStorage.getItem(TOKEN)
      } else if (SessionStorage.has(TOKEN)) {
        jwt = SessionStorage.getItem(TOKEN)
      }
    }
    return jwt === null ? jwt : jwt.access_token
  },
  setJwt (remember, value) {
    if (process.env.CLIENT) {
      remember
        ? LocalStorage.set(TOKEN, value)
        : SessionStorage.set(TOKEN, value)
    }
  },
  removeJwt () {
    if (process.env.CLIENT) {
      if (LocalStorage.has(TOKEN)) {
        LocalStorage.remove(TOKEN)
      } else if (SessionStorage.has(TOKEN)) {
        SessionStorage.remove(TOKEN)
      }
    }
  },
  setHeaders (headers = {}) {
    const jwt = this.getJwt()
    const h = {
      ...headers,
      Accept: 'application/json',
      'X-Requested-With': 'XHR',
      'Content-Type': 'application/json'
    }
    if (jwt) {
      h.Authorization = `Bearer ${jwt}`
    }
    return h
  },
  send (url, method, body, headers) {
    const controller = new AbortController()
    const init = {
      method: method,
      mode: 'cors',
      credentials: 'include',
      cache: 'no-cache',
      headers: this.setHeaders(headers),
      signal: controller.signal
    }
    if (body !== null) {
      init.body = JSON.stringify(body)
    }
    return new Observable(observer => {
      LoadingBar.start()
      fetch(url, init)
        .then(response => this.handleResponse(response))
        .then(json => observer.next(json))
        .catch(error => {
          observer.next(error)
          // return observer.complete()
        })
        .finally(() => {
          LoadingBar.stop()
          observer.complete()
        })
      return () => controller.abort()
    })
  },
  get$ (url, headers = {}) {
    return this.send(url, 'GET', null, headers)
  },
  delete$ (url, data = {}, headers = {}) {
    return this.send(url, 'DELETE', data, headers)
  },
  post$ (url, data, headers = {}) {
    return this.send(url, 'POST', data, headers)
  },
  patch$ (url, data, headers = {}) {
    return this.send(url, 'PATCH', data, headers)
  },
  put$ (url, data, headers = {}) {
    return this.send(url, 'PUT', data, headers)
  },
  prefetch (url, headers = {}) {
    return new Promise((resolve, reject) => {
      const options = {
        method: 'GET',
        headers: this.setHeaders(headers)
      }
      const req = http.request(new URL(url), options, res => {
        res.setEncoding('utf8')
        let rawData = ''
        // eslint-disable-next-line no-return-assign
        res.on('data', (chunk) => rawData += chunk)
        res.on('end', () => resolve(rawData))
      })
      req.on('error', (e) => reject(e.message))
      req.end()
    })
  }
}
export default Object.freeze(api)
