const mutations = {
  setSignUpStep$: (state, payload$) => (state.signUpStep$ = payload$),
  setPagination$: (state, payload$) => (state.pagination$ = payload$),
  setStudents$: (state, payload$) => (state.students$ = payload$),
  setChangeCourse$: (state, payload$) => (state.isChangeCourse$ = payload$),
  setStudent$: (state, payload$) => (state.student$ = payload$),
  setRequestPagination$: (state, payload$) => (state.requestPagination$ = payload$),
}

export default Object.freeze(mutations)
