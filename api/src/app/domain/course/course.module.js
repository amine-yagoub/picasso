import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { CourseSchema } from "./course.schema";
import CourseController from "../../controllers/course.controller";
import CourseService from "../../services/course.service";
import { SectionSchema } from "./section.schema";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'courses', schema: CourseSchema }]),
    MongooseModule.forFeature([{ name: 'sections', schema: SectionSchema }]),
  ],
  controllers: [CourseController],
  providers: [CourseService]
})
class CourseModule {}
export default CourseModule
