import app from "src/app/services/app";

export const common = ["code", "name", "phones", "course", "created", "actions"]
export const clients = ["proceduresDate", "employeeInCharge", "lastNote"]
export const students = [
   "section",
  "picture",
  "numberOfInstallments",
  "currentInstallment",
  "numberOfPaidInstallments",
  "installmentDate",
  "absentsCount"
  ]

export default {
  name: "employeeStudentsData",
  meta () {
    return {
      title: app.titleTemplate(this.$t("headers.registerFirstStep")),
      // meta tags
      meta: {
        name: { name: "robots", content: "noindex,nofollow" }
      }
    };
  },
  methods: {
    getLastNote(notes) {
      return notes.length === 0 ? this.$t('text.noNotes') : notes.pop().title
    },
    getCourseName(course) {
      return course[0]['name'] ?? null
    }
  },
  data () {
    return {
      filter: "",
      selected: [],
      currentList: "الطلاب العملاء",
      visibleColumns: [...common, ...clients],
      studentsList: [
        {
          label: "الطلاب العملاء",
          count: 5,
          role: "client"
        },
        {
          label: "قائمة أرشيف العملاء",
          count: 0,
          role: "clientArchive"
        }, {
          label: "قائمة مواعيد العملاء",
          count: 5,
          role: "clientVisit"
        },
        {
          label: "الطلاب المتدربين الجدد",
          count: 0,
          role: "newStudent"
        }, {
          label: "الطلاب المتدربين",
          count: 0,
          role: "student"
        }, {
          label: "الطلاب المتوقع تخرجهم",
          count: 0,
          role: "almostDone"
        }, {
          label: "الطلاب المنتهين",
          count: 0,
          role: "done"
        }, {
          label: "الطلاب الخريجين",
          count: 0,
          role: "graduated"
        },
        {
          label: "الطلاب المنقطعين و المفصولين",
          count: 0,
          role: "banned"
        }
      ],
      columns: [
        {
          name: "code",
          label: this.$t("labels.fileCode"),
          field: "code",
          align: "center"
        },
        {
          name: "picture",
          align: "center",
          label: this.$t("labels.picture"),
          field: "picture"
        },
        {
          name: "name",
          required: true,
          label: this.$t("labels.fullName"),
          align: "center",
          field: row => row,
          sortable: true
        },
        {
          name: "phones",
          align: "center",
          field: "phone",
          label: this.$t("labels.phones")
        },
        {
          name: "course",
          align: "center",
          label: this.$t("labels.course"),
          field: row => this.getCourseName(row.courses),
          sortable: true
        },
        {
          name: "created",
          align: "center",
          label: this.$t("labels.created"),
          field: row => app.formatDate(row.created),
          sortable: true
        },
        {
          name: "proceduresDate",
          align: "center",
          label: this.$t("labels.proceduresDate"),
          field: row => app.formatDate(row.proceduresDate),
          sortable: true
        },
        {
          name: "employeeInCharge",
          align: "center",
          label: this.$t("labels.employeeInCharge"),
          field: row => row.employeeInCharge.name,
          sortable: true
        },
        {
          name: "lastNote",
          align: "center",
          label: this.$t("labels.lastNote"),
          field: row => this.getLastNote(row.notes),
          sortable: true
        },
        {
          name: "section",
          align: "center",
          label: this.$t("labels.section"),
          field: row => this.getCourseName(row.sections),
          sortable: true
        },
        {
          name: "numberOfInstallments",
          align: "center",
          label: this.$t("labels.numberOfInstallments"),
          field: "courses",
          sortable: true
        },
        {
          name: "currentInstallment",
          align: "center",
          label: this.$t("labels.currentInstallment"),
          field: () => 0,
        },
        {
          name: "numberOfPaidInstallments",
          align: "center",
          label: this.$t("labels.numberOfPaidInstallments"),
          sortable: true,
          field: () => 1,
        },
        {
          name: "installmentDate",
          align: "center",
          label: this.$t("labels.installmentDate"),
          sortable: true,
          field: () => 'time',
        },
        {
          name: "absentsCount",
          align: "center",
          label: this.$t("labels.absentsCount"),
          sortable: true,
          field: row => row.absents.length,
        },
        {
          name: "actions",
          align: "center",
          label: this.$t("labels.actions"),
          field: row => row
        }
      ]
    };
  }
};
