const state = () => ({
  pagination$: null,
  sections$: null,
  courses$: null,
  course$: null,
  requestPagination$: null,
})
export default state
