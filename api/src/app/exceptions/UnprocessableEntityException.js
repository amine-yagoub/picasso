import { HttpException, HttpStatus } from '@nestjs/common'

export class UnprocessableEntityException extends HttpException {
  constructor (messages) {
    super({
      statusCode: HttpStatus.UNPROCESSABLE_ENTITY,
      error: 'Unprocessable Entity',
      message: messages
    }, HttpStatus.UNPROCESSABLE_ENTITY)
  }
}
