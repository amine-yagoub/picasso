const mutations = {
  setAvatar$: (state, payload$) => (state.avatar$ = payload$),
  setUser$: (state, payload$) => (state.user$ = payload$)
}
export default Object.freeze(mutations)
