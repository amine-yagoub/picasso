import { Dependencies, Injectable } from "@nestjs/common";
import { getModelToken } from "@nestjs/mongoose";

@Dependencies(getModelToken('courses'), getModelToken('sections'))
@Injectable()
class CourseService{
  /**
   * CourseService Constructor
   *
   * @param courseModel
   * @param sectionModel
   */
  constructor (courseModel, sectionModel) {
    this.courseModel = courseModel
    this.sectionModel = sectionModel
  }
  
  /**
   * Find all courses.
   *
   * @returns {Promise<*>}
   */
  async findAll() {
    return await this.courseModel.find().lean()
  }
  
  /**
   * Find all courses.
   *
   * @returns {Promise<*>}
   */
  async findCourseSections(id) {
    console.log(id)
    return await this.sectionModel.find({course: id}).lean()
  }
  
}
export default CourseService
