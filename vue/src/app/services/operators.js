import { store } from 'src/store'
import { tap } from 'rxjs/operators'

export function showSpinner () {
  return tap(() => store.state.app.loading$.next(true))
}
export function hideSpinner () {
  return tap(() => store.state.app.loading$.next(false))
}

export function showPageLoading () {
  return tap(() => store.state.app.pageLoading$.next(true))
}
export function hidePageLoading () {
  return tap(() => store.state.app.pageLoading$.next(false))
}
