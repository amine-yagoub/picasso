import {
  Controller,
  Dependencies,
  Post,
  Get,
  Body,
  Bind,
  Param,
  Request,
  UseGuards,
  UseInterceptors,
  UnauthorizedException, Logger, BadRequestException, Delete, Req
} from "@nestjs/common";
import AuthService from "../services/auth.service";

import { UnprocessableEntityException } from "../exceptions/UnprocessableEntityException";
import NonceInterceptor from "../interceptors/nonce.interceptor";
import JwtAuthGuard from "../guards/jwtAuth.guard";
import MailService from "../services/mail.service";

@Controller("auth")
@Dependencies(AuthService, MailService)
class AuthController {
  /**
   *
   * @param authService
   * @param mailService
   */
  constructor (authService, mailService) {
    this.authService = authService;
    this.mailService = mailService;
  }
  
  /**
   * SignUp the user.
   *
   * @param body
   * @returns {Promise<*>}
   */
  @Post("register")
  @Bind(Body())
  signUp (body) {
    return this.authService.tryToSignUp(body)
      .then(() => {
        this.mailService.send({
          path: "auth/signUp",
          vars: { emailToken: body.emailToken },
          to: body.email,
          subject: "Activate your account"
        });
        return { message: "Created", statusCode: 201 };
      })
      .catch(error => {
        Logger.error(error);
        if (error.code === 11000) {
          throw new UnprocessableEntityException({ email: "exist" });
        }
        throw new BadRequestException();
      });
  }
  
  /**
   * SignIn the user.
   *
   * @param request
   * @returns {Promise<*>}
   */
  @Post("login")
  @Bind(Request())
  @UseInterceptors(NonceInterceptor)
  async signIn (request) {
    try {
      const data = await this.authService.tryToLogin(request);
      if (data) {
        return data;
      }
    } catch (e) {
      Logger.error(e);
      throw new UnauthorizedException();
    }
    throw new UnauthorizedException();
  }
  
  /**
   * Confirm user account.
   *
   * @param token
   * @param request
   *
   * @returns {Promise<{jwt: {access_token: *, id_token: *, token_type: string, expires_in: *}, data: *}>}
   */
  @Get("verify/:token")
  @Bind(Param("token"), Request())
  confirmUserEmail (token, request) {
  }
  
  @UseGuards(JwtAuthGuard)
  @Get('user-info')
  @Bind(Req())
  getUserInfo(req) {
    return req.user;
  }
  
  /**
   *
   * @returns {string}
   */
  @Delete("seed")
  deleteMany () {
    return this.authService.deleteMany();
  }
}

export default AuthController;
