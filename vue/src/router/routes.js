const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout"),
    children: [
      {
        path: "",
        component: () => import("pages/Index"),
        name: 'home'
      },
    ]
  },
  {
    path: "/auth",
    component: () => import("layouts/AuthLayout"),
    children: [
      {
        path: "login",
        component: () => import("pages/auth/Login"),
        name: 'login'
      },
      {
        path: "me",
        component: () => import("pages/auth/Me"),
        name: 'me',
        meta: { requiresAuth: true, visible: true }
      },
    ]
  },
  {
    path: "/students",
    name: "students",
    component: () => import("layouts/DashboardLayout"),
    children: [
      { path: "register", name: "studentSignUp", component: () => import("pages/auth/StudentSignUp") }
    ]
  },
  {
    path: "/employees",
    component: () => import("layouts/DashboardLayout"),
    children: [
      { path: "", name: "employeeDashboard", component: () => import("pages/employee/EmployeeDashboard") },
      {
        path: "manage-students",
        name: "employeeManageStudents",
        component: () => import("pages/employee/EmployeeManageStudents"),
      },
      {
        path: "manage-students/create",
        name: "employeeCreateStudent",
        component: () => import("pages/auth/StudentSignUp")
      },
      {
        path: "manage-students/:code",
        name: "studentProfile",
        component: () => import("pages/student/StudentProfile"),
        meta: { requiresAuth: false, visible: true, role: "employee" },
        props: true
      },
      {
        path: "manage-courses",
        name: "employeeManageCourses",
        component: () => import("pages/employee/EmployeeManageCourses")
      },
      {
        path: "notifications",
        name: "employeeNotifications",
        component: () => import("pages/employee/EmployeeNotifications")
      },
      {
        path: "certificates",
        name: "employeeCertificates",
        component: () => import("pages/employee/EmployeeCertificates")
      },
      {
        path: "examinations",
        name: "employeeExaminations",
        component: () => import("pages/employee/EmployeeExaminations")
      },
      {
        path: "quality-of-training",
        name: "employeeQualityOfTraining",
        component: () => import("pages/employee/EmployeeQualityOfTraining")
      },
      {
        path: "inbox",
        name: "employeeInbox",
        component: () => import("pages/employee/EmployeeInbox")
      }
    ]
  },
  {
    path: "/errors/500",
    component: () => import("pages/errors/Error500.vue"),
    name: 'error500'
  },
  {
    path: "*",
    component: () => import("pages/errors/Error404.vue")
  }
];

export default routes;
