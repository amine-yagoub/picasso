import { PassportStrategy } from '@nestjs/passport'
import { Injectable, Dependencies, UnauthorizedException } from '@nestjs/common'
import AuthService from '../../services/auth.service'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { ConfigService } from '@nestjs/config'

@Injectable()
@Dependencies(AuthService, ConfigService)
class JwtStrategy extends PassportStrategy(Strategy) {
  constructor (authService, configService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get('jwt.key'),
      aud: configService.get('jwt.aud'),
      iss: configService.get('jwt.iss')
    })
    this.authService = authService
  }

  /**
   * Validate Jwt payload.
   *
   * @param payload
   * @param done
   * @returns {Promise<*>}
   */
  async validate (payload, done) {
    try {
      const user = await this.authService.validateUserToken(payload)
      if (!user) {
        return done(new UnauthorizedException(), false)
      }
      done(null, user)
    }catch (e) {
      return done(new UnauthorizedException(), false)
    }
  }
}
export default JwtStrategy
