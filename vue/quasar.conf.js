const fs = require('fs')
module.exports = function (ctx) {
  return {
    supportTS: false,
    preFetch: true,
    boot: ['i18n', 'rxjs', 'auth', 'libs'],
    css: ['app.styl'],
    extras: [
      'roboto-font',
      'material-icons'
    ],
    build: {
      env: ctx.dev
        ? {
          // so on dev we'll have
          API: 'http://localhost',
          WS: 'ws://localhost',
          API_PORT: 9000,
          DS_PORT: 9030
        }
        : {
          // and on build (production):
          API: 'https://api.slashDelivery.com',
          WS: 'wss://slashDelivery.com',
          API_PORT: 433,
          DS_PORT: 9030
        },
      vueRouterMode: 'history',
      rtl: true,
      // preloadChunks: true,
      // showProgress: false,
      // gzip: true,
      // analyze: true,
      // extractCSS: false,
      extendWebpack (cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules/
        })
      }
    },
    devServer: {
      https: {
        key: fs.readFileSync('C:\\Windows\\System32\\localhost+1-key.pem'),
        cert: fs.readFileSync('C:\\Windows\\System32\\localhost+1.pem'),
        ca: fs.readFileSync('C:\\Users\\Amine\\AppData\\Local\\mkcert\\rootCA.pem')
      },
      port: 8080,
      open: true
    },
    framework: {
      iconSet: 'material-icons',
      lang: 'ar',
      importStrategy: 'auto',
      directives: ['ClosePopup'],
      plugins: [
        'Meta',
        'LoadingBar',
        'Notify',
        'Dialog',
        'LocalStorage',
        'SessionStorage'
      ],
      config: {
        LoadingBar: {
          position: 'top'
        },
        notify: {
          position: 'bottom-left',
          timeout: 3000,
          textColor: 'white',
          actions: [{ icon: 'close', color: 'white' }]
        }
      }
    },
    ssr: {
      pwa: false
    },
    pwa: {
      workboxPluginMode: 'GenerateSW',
      workboxOptions: {},
      manifest: {
        name: 'Slash CraftsMan',
        short_name: 'Slash CraftsMan',
        description: 'CraftsMan Skills Share App',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#fff',
        theme_color: '#027be3',
        icons: [
          {
            src: 'icons/icon-128x128.png',
            sizes: '128x128',
            type: 'image/png'
          },
          {
            src: 'icons/icon-192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src: 'icons/icon-256x256.png',
            sizes: '256x256',
            type: 'image/png'
          },
          {
            src: 'icons/icon-384x384.png',
            sizes: '384x384',
            type: 'image/png'
          },
          {
            src: 'icons/icon-512x512.png',
            sizes: '512x512',
            type: 'image/png'
          }
        ]
      }
    },
    cordova: {},
    capacitor: {
      hideSplashscreen: true
    },
    electron: {
      bundler: 'packager',
      packager: {},
      builder: {
        appId: 'craftsman'
      },
      nodeIntegration: true,
      extendWebpack (/* cfg */) {
        // do something with Electron main process Webpack cfg
        // chainWebpack also available besides this extendWebpack
      }
    }
  }
}
