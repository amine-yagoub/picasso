const state = () => ({
  config: null,
  route$: null,
  notify$: null,
  loading$: null,
  pageLoading$: null,
  broadcastChannel: null,
  time: 0,
  labels: {}
})
export default state
