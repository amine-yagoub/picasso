import { concatMap, pluck, tap, throttleTime } from "rxjs/operators"
import { showSpinner, hideSpinner } from 'src/app/services/operators'
import student from "src/app/services/student";
import course from "src/app/services/course";
export default {
  name: 'registerStudentThirdStep',
  domStreams: ['onChooseSection$'],
  subscriptions () {
    return {
      submitForm$: this.onSubmitForm(),
      sections$: this.getCourseSections()
    }
  },
  data () {
    return {
      section: {
        firstInstallmentAmount: 0,
        startDate: null,
        endDate: null
      }
    }
  },
  model: {
    prop: 'student$'
  },
  props: {
    loading$: {
      type: Boolean,
      required: true
    },
    student$: {
      type: Object,
      required: true
    }
  },
  methods: {
    getCourseSections() {
      return course.getSections$(this.student$.course._id).pipe(
        tap(result => {
          if(!Array.isArray(result)) {
            this.getNotify$.next('getDataFail')
            // redirect to user profile
          }
        })
      )
    },
    onSubmitForm () {
      return this.onChooseSection$.pipe(
        throttleTime(1500),
        showSpinner(),
        pluck('data'),
        concatMap(data => {
          const { _id, course, name } = data.section
          const { startDate, endDate } = this.section
          data.section = {
            id: _id,
            name,
            course,
            startDate,
            endDate
          }
          return student.updateClientToNewStudent$(data)
        }),
        hideSpinner(),
        tap(({ student, pwd }) => {
          if(student) {
            student.password = pwd
            this.$store.getters['student/getStudent$'].next(student)
            return this.getStep$.next(4)
          }
          this.getNotify$.next('generalError')
        })
      )
    },
  }

}
