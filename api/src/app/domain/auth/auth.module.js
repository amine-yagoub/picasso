import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import AuthController from '../../controllers/auth.controller'
import AuthService from '../../services/auth.service'
import { AuthSchema } from './auth.schema'
import { PassportModule } from '@nestjs/passport'
import { JwtModule } from '@nestjs/jwt'
import { ConfigModule, ConfigService } from '@nestjs/config'
import JwtStrategy from './jwt.strategy'
import MailService from '../../services/mail.service'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'auth', schema: AuthSchema }]),
    PassportModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService) => {
        return {
          secret: configService.get('jwt.key'),
          signOptions: { expiresIn: configService.get('jwt.expiresIn') }
        }
      },
      inject: [ConfigService]
    })
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, MailService, ConfigService]
})
class AuthModule {}
export default AuthModule
