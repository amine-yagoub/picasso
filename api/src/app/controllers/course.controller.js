import {
  Controller,
  Dependencies,
  Post,
  Get,
  Body,
  Bind,
  Param,
  Request,
  UseGuards,
  UseInterceptors,
  UnauthorizedException, Logger, BadRequestException, Delete
} from '@nestjs/common'

import CourseService from "../services/course.service";

@Controller('courses')
@Dependencies(CourseService)
class CourseController {
  /**
   *
   * @param courseService
   */
  constructor (courseService) {
    this.courseService = courseService
  }

  /**
   * Get all courses.
   *
   * @returns {Promise<*>}
   */
  @Get()
  async getAll () {
    try {
      return await this.courseService.findAll()
    } catch (error) {
      Logger.error(error)
    }
  }
  
  /**
   * Get all courses.
   *
   * @returns {Promise<*>}
   */
  @Get(':id/sections')
  @Bind(Param('id'))
  async getAllSections (id) {
    try {
      return await this.courseService.findCourseSections(id)
    } catch (error) {
      Logger.error(error)
    }
  }
}
export default CourseController
