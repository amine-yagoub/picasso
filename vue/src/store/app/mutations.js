const mutations = {
  setConfig$: (state, payload$) => (state.config = payload$),
  setNotify$: (state, payload$) => (state.notify$ = payload$),
  setLoading$: (state, payload$) => (state.loading$ = payload$),
  setPageLoading$: (state, payload$) => (state.pageLoading$ = payload$),
  setRoute$: (state, payload$) => (state.route$ = payload$),
  updateTime: state => (state.time += 1000 * 60),
  setTime: (state, payload) => (state.time = payload),
  initBroadcastChannel: state =>
    (state.broadcastChannel = new BroadcastChannel('login'))
}

export default Object.freeze(mutations)
