import { BehaviorSubject } from "rxjs";
import { helper, publicRoutes, errorPages } from "./helper";
import { first } from "rxjs/operators";

export default async ({ store, router }) => {
  store.commit("auth/setUser$", new BehaviorSubject({}));
  store.commit("app/setRoute$", new BehaviorSubject({}));
  store.commit("app/setNotify$", new BehaviorSubject({}));

  router.beforeEach((to, from, next) => {
    const user$ = store.getters["auth/getUser$"]
    const route$ = store.getters["app/getRoute$"]
    let auth
    helper.fetchUser$(user$).pipe(first()).subscribe(async user => {
      auth = await user
      store.getters["auth/getUser$"].next(auth)
      if(auth.statusCode === 500) {
        return route$.next({
          name: "error500"
        });
      }
      if(auth.statusCode === 401) {
        // Here because nonce is invalid
        route$.next({
          name: "login"
        });
        return store.getters["app/getNotify$"].next("youNeedToLogin")
      }
      const { _id } = auth
      if (Object.values(errorPages).includes(to.name) === false) {
        try {
          switch (true) {
            case publicRoutes.includes(to.name) && _id:
              route$.next({
                name: "home"
              });
              break;
            case to.matched.some(({ meta }) => meta.requiresAuth) && !_id:
              route$.next({
                name: "login",
                query: { redirect: to.fullPath }
              });
              store.getters["app/getNotify$"].next("youNeedToLogin");
              break;
            case !to.matched.some(({ meta }) => meta.meta.visible):
              route$.next({
                name: "cowboy"
              });
              break;
            default:
              break;
          }
        } catch (error) {
          next({
            name: errorPages[error.status]
          });
        }
      }
      next();
    })
  });
};
