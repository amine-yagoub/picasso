import Api from './api'
const COURSES = `${Api.getBaseUri()}/courses`
// const SIGN_IN_ROUTE = `${AUTH}/login`
// const SIGN_OUT_ROUTE = `${AUTH}/logout`
// const PASSWORD_FORGOT_ROUTE = `${AUTH}/forgot`
// const PASSWORD_RECOVER_ROUTE = `${AUTH}/recover`
// const ACTIVATE_ACCOUNT_ROUTE = `${AUTH}/verify`
// const USER_INFO_ROUTE = `${AUTH}/user-info`
// const CSRF_ROUTE = `${AUTH}/csrf-token`
// const RESEND_ACTIVATION_ROUTE = `${AUTH}/resend-activation`

export default Object.freeze({
  getCourses$: () => Api.get$(COURSES),
  getSections$: (id) => Api.get$(`${COURSES}/${id}/sections`)
})
