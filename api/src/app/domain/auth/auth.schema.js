import { Schema } from 'mongoose'

export const AuthSchema = new Schema({
  phone: {
    type: String,
    required: true,
    index: { unique: true }
  },
  password: {
    type: String,
    required: true
  },
  verified: {
    type: Boolean,
    default: false
  },
  platform: {
    type: Object
  },
  lastLogin: {
    type: Date,
    default: Date.now()
  },
  // Dynamic References via `refPath`
  model: {
    type: Schema.Types.ObjectId,
    required: true,
    refPath: 'onModel'
  },
  onModel: {
    type: String,
    required: true,
    enum: ['students', 'employees']
  }
  // profile: ProfileSchema  ----Embed one document => user.profile = new ProfileSchema({})
  // courses: [CourseSchema] ----Embed array of documents
  // courses: [{ type: Schema.Types.ObjectId, ref: 'Courses'}] ----Embed array of ids of documents => use populate to findAll them
})
/*
AuthSchema.virtual('courseCount').get(function(){ return this.courses.length })

AuthSchema.pre('remove', function(next) {
  const courses = mongoose.model('courses')
  courses.remove({_id: {$in: this.courses}}).then(next())
})
*/
