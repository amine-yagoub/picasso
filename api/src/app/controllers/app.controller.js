import { Controller, Dependencies, Get } from '@nestjs/common'
import AppService from '../services/app.service'

@Controller()
@Dependencies(AppService)
class AppController {
  /**
   * AppController Constructor
   *
   * @param appService
   */
  constructor (appService) {
    this.appService = appService
  }

  @Get()
  /**
   *
   * @returns {string}
   */
  getHello () {
    return this.appService.getHello()
  }
}
export default AppController
