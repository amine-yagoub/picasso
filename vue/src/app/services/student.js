import Api from './api'
const STUDENTS = `${Api.getBaseUri()}/students`
const SIGN_UP = `${STUDENTS}/register`
import pagination from './pagination'
// const SIGN_IN_ROUTE = `${AUTH}/login`
// const SIGN_OUT_ROUTE = `${AUTH}/logout`
// const PASSWORD_FORGOT_ROUTE = `${AUTH}/forgot`
// const PASSWORD_RECOVER_ROUTE = `${AUTH}/recover`
// const ACTIVATE_ACCOUNT_ROUTE = `${AUTH}/verify`
// const USER_INFO_ROUTE = `${AUTH}/user-info`
// const CSRF_ROUTE = `${AUTH}/csrf-token`
// const RESEND_ACTIVATION_ROUTE = `${AUTH}/resend-activation`

export default Object.freeze({
  signUpClient$: data => Api.post$(SIGN_UP, data),
  updateClientToNewStudent$: data => Api.put$(SIGN_UP, data),
  paginateStudents$: options => Api.get$(pagination.buildLink(options, STUDENTS)),
  getStudentByCode$: code => Api.get$(`${STUDENTS}/${code}`),
})
