import { JwtService } from '@nestjs/jwt'
import { ConfigService } from '@nestjs/config'
import { getModelToken } from '@nestjs/mongoose'
import AuthHelperService from './authHelper.service'
import { Injectable, Dependencies } from '@nestjs/common'
import validator from './validator.service'
import { UnprocessableEntityException } from '../exceptions/UnprocessableEntityException'

@Dependencies(getModelToken('auth'), JwtService, ConfigService)
@Injectable()
class AuthService extends AuthHelperService {
  /**
   * AuthService Constructor
   *
   * @param authModel
   * @param jwtService
   * @param configService
   */
  constructor (authModel, jwtService, configService) {
    super(configService)
    this.jwtService = jwtService
    this.AuthModel = authModel
    this.configService = configService
  }
  
  /**
   * Generate Jwt token.
   *
   * @param user
   * @param rawHeaders
   * @returns {{jwt: {access_token: *, id_token: *, token_type: string, expires_in: *}, data: *}}
   */
  async generateJwtForUser (rawHeaders, user) {
    const uuid = this.uuid()
    const nonce = await this.encryptNonce(rawHeaders)
    const { _id, role, name, phone } = user
    const payload = {
      sub: _id,
      jti: uuid,
      iat: Math.floor(Date.now() / 1000),
      nonce,
      phone,
      role
    }
    return {
      nonce,
      jwt: {
        id_token: uuid,
        access_token: this.jwtService.sign(payload),
        expires_in: this.configService.get('jwt.expiresIn'),
        token_type: 'bearer'
      },
      data: {
        _id,
        role,
        name
      }
    }
  }

  /**
   * Find user by phone.
   *
   * @param phone
   * @returns {Promise<*|void|string>}
   */
  async findByPhone (phone) {
    return this.AuthModel.findOne({ phone }).populate('model')
  }

  /**
   * Find user by token.
   *
   * @param id
   * @returns {Promise<*|void|string>}
   */
  async findById (id) {
    return this.AuthModel.findOne({ _id: id })
  }

  /**
   * Validate user login credentials.
   *
   * @param body
   * @returns {Promise<>}
   */
  async validateLoginCredentials (body) {
    const { phone, password } = body
    const user = await this.findByPhone(phone)
    if (user && await this.pwdVerify(password, user.password)) {
      return this.getUserData(user);
    }
    return null
  }
  
  /**
   *
   * @param user
   * @param toObject
   * @returns {{lastLogin: *}}
   */
  getUserData (user, toObject = false) {
    const { lastLogin, model } = toObject ? user.toObject() : user;
    return { lastLogin, ...model };
  }
  
  /**
   * Validate payload and nonce.
   *
   * @param payload
   * @returns {Promise<*>}
   */
  async validateUserToken (payload) {
    return await this.validateNonce(payload)
      ? this.getUserData(await this.findByPhone(payload.phone), true)
      : Promise.reject(new Error('Invalid user Token'))
  }

  /**
   * Sign In the User.
   *
   * @param { raw, body }
   * @returns {Promise<*>}
   */
  async tryToLogin ({ headers, body }) {
    const user = await this.validateLoginCredentials(body)
    return user
      ? this.generateJwtForUser(this.getNonce(headers), user._doc)
      : Promise.reject(new Error('Invalid login credentials'))
  }
  

  /**
   *
   * Sign Up the User.
   *
   * @param body
   * @returns {Promise<any>}
   */
  async tryToSignUp (body) {
    this.validateSignUpData(body)
    body.password = await this.pwdHash(body.password)
    body.emailToken = this.uuid()
    body.emailTokenExpire = 24
    return await new this.AuthModel(body).save()
  }

  /**
   * Validate the request body.
   *
   * @param body
   */
  validateSignUpData (body) {
    const { email, required, password, alphaNum, between } = validator
    const minName = this.configService.get('api.validation.minNameChar')
    const maxName = this.configService.get('api.validation.maxNameChar')
    const minPwd = this.configService.get('api.validation.minPwdChar')
    const maxPwd = this.configService.get('api.validation.maxPwdChar')
    const errors = {}
    switch (true) {
      case !body.acceptTerms:
        errors.acceptTerms = 'invalid'
      case !this.platformParamInvalid(body.platform):
        errors.platform = 'invalid'
      case !['student', 'instructor'].includes(body.role):
        errors.role = 'invalid'
      case !email(body.email):
        errors.email = 'invalid'
      case !required(body.name):
        errors.name = 'required'
      case !between(body.name, minName, maxName):
        errors.name = 'length'
      case !alphaNum(body.name):
        errors.name = 'invalid'
      case !password(body.password):
        errors.password = 'invalid'
      case !between(body.password, minPwd, maxPwd):
        errors.password = 'length'
      case body.password !== body.repeatPassword:
        errors.repeatPassword = 'invalid'
    }
    if (Object.keys(errors).length) {
      throw new UnprocessableEntityException(errors)
    }
  }

  /**
   * Check if the platform param have all necessary keys.
   * @param values
   * @returns {boolean}
   */
  platformParamInvalid (values) {
    return !!values && ['platform', 'desktop', 'name', 'version'].every((v) => Object.keys(values).includes(v))
  }

  /**log
   *
   * @returns {*}
   */
  deleteMany () {
    return this.AuthModel.deleteMany()
  }
}
export default AuthService
