import { of } from "rxjs";

const state = () => ({
  avatar$: null,
  user$: null
})
export default state
