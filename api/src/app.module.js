import { Logger, Module } from '@nestjs/common'
import AppController from './app/controllers/app.controller'
import AppService from './app/services/app.service'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { MongooseModule } from '@nestjs/mongoose'

import api from './config/api'
import db from './config/db'
import jwt from './config/jwt'
import AuthModule from './app/domain/auth/auth.module'
import StudentModule from "./app/domain/student/student.module"
import CourseModule from "./app/domain/course/course.module"
import EmployeeModule from "./app/domain/employee/employee.module"

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [api, db, jwt]
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService) => ({
        uri: configService.get('database.uri'),
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
      }),
      inject: [ConfigService]
    }),
    AuthModule,
    CourseModule,
    StudentModule,
    EmployeeModule
  ],
  controllers: [AppController],
  providers: [AppService, Logger]
})
class AppModule {}
export default AppModule
