import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import StudentSchema from "./student.schema";
import StudentController from "../../controllers/student.controller";
import StudentService from "../../services/student.service";
import { ConfigService } from "@nestjs/config";
import { AuthSchema } from "../auth/auth.schema";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'students', schema: StudentSchema }]),
    MongooseModule.forFeature([{ name: 'auth', schema: AuthSchema }])
  ],
  controllers: [StudentController],
  providers: [StudentService, ConfigService]
})
class StudentModule {}
export default StudentModule
