const getters = {
  getAvatar$: state => state.avatar$,
  getUser$: state => state.user$
}
export default Object.freeze(getters)
