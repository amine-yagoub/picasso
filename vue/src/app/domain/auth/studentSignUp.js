import app from 'src/app/services/app'
export default {
  name: 'studentSignUp',
  meta () {
    return {
      title: app.titleTemplate(this.$t('buttons.addNewStudent')),
      // meta tags
      meta: {
        name: { name: 'robots', content: 'noindex,nofollow' }
      }
    }
  },
  domStreams: ['signUp$'],
  subscriptions () {
    const { getters } = this.$store;
    return {
      loading$: getters['app/getLoading$'],
      student$: getters["student/getStudent$"],
      step$: getters['student/getSignUpStep$']
    }
  },
  methods: {
    goBack () {
      this.$store.getters['student/getSignUpStep$'].next(1)
    }
  },
  computed: {
    getSignUpBtnLabel() {
      return this.step$ === 2 ? this.$t('buttons.save') : this.$t('buttons.next')
    },
    getSignUpBtnIcon() {
      return this.step$ === 2 ? 'save' : 'arrow_back_ios'
    },
    submitFor() {
      return `form${this.step$}`;
    }
  }
}
