export const init = {
  request: {
    sortBy: null,
    descending: false,
    page: 1,
    role: 'client',
    rowsPerPage: 5,
    rowsNumber: 0,
    links: null,
    isNext: false,
    isPrev: false
  },
  current: {}
};
export default Object.freeze({
  updateSort(link, { route, limit, perPage, sort, sortBy, order }) {
    let newSort = `${sort}${order}${sortBy}`;
    let sortRegex = new RegExp(`${sort}(-)?(\\w*)`, "g");
    let oldSort = sortRegex.exec(link);
    if (!oldSort && sortBy) {
      link = `${route}?${limit}${perPage}&${newSort}`;
    }
    if (oldSort && oldSort[2] !== sortBy) {
      link = `${route}?${limit}${perPage}&${newSort}`;
    }
    return link;
  },
  addLimitAndSort({ route, limit, perPage, sort, sortBy, order }, link) {
    let limitRegex = new RegExp(`${limit}(\\d)*`, "g");
    let oldLimit = link.match(limitRegex);
    let newLimit = `${limit}${perPage}`;
    if (oldLimit[0] !== newLimit) {
      link = `${route}?${newLimit}`;
    }
    if (sortBy) {
      link = `${link}&${sort}${order}${sortBy}`;
    }
    return link;
  },
  buildLink(options, url) {
    const limit = "limit=";
    const perPage = options.rowsPerPage;
    let link = `${url}?${limit}${perPage}`;
    const params = {
      url,
      limit,
      perPage,
      sort: "sort=",
      sortBy: options.sortBy,
      order: options.descending ? "-" : ""
    };
    if (options.isNext === true) {
      link = options.links.next;
    } else if (options.isPrev === true) {
      link = options.links.prev;
    } else {
      link = this.addLimitAndSort(params, link);
    }
    link = this.updateSort(link, params);
    if (options.filter) {
      link += `&search=${options.filter}`;
    }
    if (options.role) {
      link += `&role=${options.role}`;
    }
    return link;
  }
});
