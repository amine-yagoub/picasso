import { Dependencies, Injectable } from "@nestjs/common";
import { getModelToken } from "@nestjs/mongoose";

@Dependencies(getModelToken('employees'))
@Injectable()
class EmployeeService{
  /**
   * AuthService Constructor
   *
   * @param employeeModel
   */
  constructor (employeeModel) {
    this.employeeModel = employeeModel
  }
  
  /**
   * Find all employees.
   *
   * @returns {Promise<*>}
   */
  async findAll() {
    return await this.employeeModel.find().lean()
  }
}
export default EmployeeService
