import { NestFactory } from '@nestjs/core'
import { FastifyAdapter } from '@nestjs/platform-fastify'
import AppModule from './app.module'
// import { WinstonModule } from 'nest-winston'
// import winston from 'winston'
const fastify = new FastifyAdapter()
const fileUpload = require('fastify-file-upload')
fastify.register(fileUpload)
async function bootstrap () {
  // const logDir = 'storage/logs'
  const app = await NestFactory.create(AppModule, fastify, {
    /* logger: WinstonModule.createLogger({
      format: winston.format.json(),
      defaultMeta: { service: 'user-service' },
      transports: [
        new winston.transports.File({ filename: `${logDir}/info.log`, level: 'info' }),
        new winston.transports.File({ filename: `${logDir}/warning.log`, level: 'warning' }),
        new winston.transports.File({ filename: `${logDir}/error.log`, level: 'error' }),
        new winston.transports.File({ filename: `${logDir}/log.log` })
      ]
    }) */
    logger: console
  })
  app.enableCors({
    origin: 'https://localhost:8080',
    maxAge: 86400,
    methods: ['GET', 'HEAD', 'OPTIONS', 'POST', 'PUT', 'PATCH', 'DELETE'],
    allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization', 'X-Csrf-Token'],
    credentials: true
  })
  await app.listen(3000, '0.0.0.0')
}

bootstrap()
