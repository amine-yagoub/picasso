import VueCroppie from "./components/Component";

export { VueCroppie };

export default {
  VueCroppie,

  install(Vue) {
    Vue.component(VueCroppie.name, VueCroppie);
    Vue.mixin({
      mounted() {
        console.log("Croppie Mounted!");
      }
    });
  }
};
