const state = () => ({
  signUpStep$: null,
  pagination$: null,
  students$: null,
  student$: null,
  isChangeCourse$: null,
  requestPagination$: null,
})
export default state
