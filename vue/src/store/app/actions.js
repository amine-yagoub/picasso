import app from 'src/app/services/app'
const actions = {
  preFetchConfig: ({ commit }) =>
    app.prefetchGeneralConfig().then(res => commit('setConfig$', res)),
  notify: (context, { name, options }) => app.notify(name, options),
  startTimer: ({ commit, getters }) => {
    commit('setTime', Math.floor(getters.getConfig.now * 1000))
    setInterval(() => commit('updateTime'), 1000 * 60)
  },
  handleBroadcastChannel: ({ rootGetters }, { data }) => {
    rootGetters['auth/getUser$'].next(data.user)
    app.setJwt(false, data.token)
  }
}

export default Object.freeze(actions)
