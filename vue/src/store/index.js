import Vue from 'vue'
import Vuex from 'vuex'
import app from './app'
import auth from './auth'
import student from './student'
import course from './course'
import employee from './employee'
Vue.use(Vuex)

let store
export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      app,
      auth,
      student,
      course,
      employee
    },
    strict: false
  })
  store = Store
  return Store
}
export { store }
