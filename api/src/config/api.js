import { registerAs } from '@nestjs/config'
import { join } from 'path'
export default registerAs('api', () => {
  return {
    port: 9000,
    protocol: 'http',
    mail: {
      host: 'mailhog',
      port: '1025',
      user: 'user',
      password: 'password',
      secure: false,
      template: join(__dirname, '../mail'),
      from: 'info@handlebars.com'
    },
    validation: {
      maxPwdChar: 64,
      minPwdChar: 8,
      maxNameChar: 30,
      minNameChar: 3
    },
    minio: {
      endPoint: 'minio',
      port: 9000,
      useSSL: false,
      accessKey: process.env.MINIO_ACCESS_KEY,
      secretKey: process.env.MINIO_SECRET_KEY
    }
  }
})
