const mutations = {
  setPagination$: (state, payload$) => (state.pagination$ = payload$),
  setCourses$: (state, payload$) => (state.courses$ = payload$),
  setSections$: (state, payload$) => (state.sections$ = payload$),
  setCourse$: (state, payload$) => (state.course$ = payload$),
  setRequestPagination$: (state, payload$) => (state.requestPagination$ = payload$),
}

export default Object.freeze(mutations)
