import { Schema } from 'mongoose'

export const EmployeeSchema = new Schema({
  name: {
    type: String,
    required: true,
    index: { unique: true }
  },
  created: {
    type: Date,
    default: Date.now()
  },
  role: {
    type: String,
    required: true,
    default: 'administrative'
  }
})
