import Vue from 'vue'
import VueRx from 'vue-rx'
import { Observable, BehaviorSubject } from 'rxjs'
import { init } from "src/app/services/pagination"

Vue.use(VueRx, { Observable })

export default ({ app, store }) => {
  app.rx = VueRx
  store.commit("app/setLoading$", new BehaviorSubject(false));
  store.commit("app/setPageLoading$", new BehaviorSubject(false));


  store.commit("employee/setEmployees$", new BehaviorSubject([]));

  store.commit("course/setCourses$", new BehaviorSubject([]));
  store.commit("course/setSections$", new BehaviorSubject([]));

  store.commit("student/setStudents$", new BehaviorSubject([]));
  store.commit("student/setPagination$", new BehaviorSubject(init.request));
  store.commit("student/setChangeCourse$", new BehaviorSubject(false));
  store.commit("student/setStudent$", new BehaviorSubject({}));
  store.commit("student/setRequestPagination$", new BehaviorSubject(init));
}
