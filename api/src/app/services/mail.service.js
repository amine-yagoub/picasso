import { Dependencies, Injectable, Logger } from '@nestjs/common'
import { createTransport } from 'nodemailer'
import { ConfigService } from '@nestjs/config'
import { readFile } from 'fs'
import { compile } from 'handlebars'

@Dependencies(ConfigService)
@Injectable()
class MailService {
  /**
   * MailService Constructor
   *
   * @param configService
   */
  constructor (configService) {
    this.configService = configService
  }

  /**
   *
   * @returns {Promise<*>}
   */
  send (options) {
    const { path, vars, to, subject } = options
    const hb = `${this.configService.get('api.mail.template')}/${path}.handlebars`
    const transporter = createTransport({
      host: this.configService.get('api.mail.host'),
      port: this.configService.get('api.mail.port'),
      secure: this.configService.get('api.mail.secure'),
      auth: {
        user: this.configService.get('api.mail.user'),
        pass: this.configService.get('api.mail.password')
      }
    })
    this.loadAndCompileTemplate(hb, vars, (error, html) => {
      if (error) {
        return Logger.error('Mail template err', error)
      }
      return transporter.sendMail({
        from: this.configService.get('api.mail.from'),
        to: to,
        subject: subject,
        html: html
      }, (err, info) => {
        if (!err) {
          Logger.log('info', 'Mail success: ' + info.response)
        } else {
          Logger.error('Mail err', err)
        }
      })
    })
  }

  /**
   * Load and parse email template.
   *
   * @param path
   * @param vars
   * @param callback
   */
  loadAndCompileTemplate (path, vars, callback) {
    readFile(path, 'utf-8', (err, html) => {
      const template = compile(html)
      err ? callback(err) : callback(null, template(vars))
    })
  }
}
export default MailService
