import { Schema } from 'mongoose'

export const SectionSchema = new Schema({
  name: {
    type: String,
    required: true,
    index: { unique: true }
  },
  created: {
    type: Date,
    default: Date.now()
  },
  course: {
    type: Schema.Types.ObjectId,
    ref: 'courses',
    required: true
  }
})
