import { registerAs } from '@nestjs/config'

export default registerAs('jwt', () => {
  return {
    key: process.env.JWT_KEY,
    expiresIn: '24h',
    aud: 'CraftsMan App',
    iss: 'http://localhost:9000/',
    nonceName: 'fgp-nonce',
    nonceAge: Math.floor((Date.now() / 1000) + (3600 * 12)),
    nonceEncryptAlg: 'aes-192-cbc',
    nonceEncryptKey: process.env.JWT_KEY
  }
})
