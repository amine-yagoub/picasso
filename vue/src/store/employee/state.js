const state = () => ({
  pagination$: null,
  employees$: null,
  employee$: null,
  requestPagination$: null,
})
export default state
