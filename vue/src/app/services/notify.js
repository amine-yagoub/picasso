import { Notify } from 'quasar'
import { i18n } from 'src/boot/i18n'

export default Object.freeze({
  networkError () {
    return Notify.create({
      color: 'warning',
      textColor: 'grey-9',
      message: i18n.t('errors.network'),
      icon: 'wifi_off'
    })
  },
  generalError () {
    return Notify.create({
      color: 'negative',
      message: i18n.t('errors.generalError'),
      icon: 'error'
    })
  },
  accountFail () {
    return Notify.create({
      color: 'negative',
      message: i18n.t('errors.accountFail'),
      icon: 'error'
    })
  },
  accountSuccess () {
    return Notify.create({
      color: 'positive',
      message: i18n.t('messages.accountCreated'),
      icon: 'how_to_reg'
    })
  },
  accountPhoneExist () {
    return Notify.create({
      color: 'negative',
      message: i18n.t('errors.accountPhoneExist'),
      icon: 'person_add_disabled'
    })
  },
  accountNameExist () {
    return Notify.create({
      color: 'negative',
      message: i18n.t('errors.accountNameExist'),
      icon: 'person_add_disabled'
    })
  },
  getDataFail () {
    return Notify.create({
      color: 'negative',
      message: i18n.t('errors.secondStepFail'),
      icon: 'error'
    })
  },
  youNeedToLogin () {
    return Notify.create({
      textColor: 'grey-9',
      color: 'warning',
      message: i18n.t('errors.loginNeeded'),
      icon: 'warning'
    })
  },
})
