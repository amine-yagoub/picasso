import Vue from "vue";
import VueCroppie from "../components/croppie";
import VueDOMPurifyHTML from "vue-dompurify-html";

Vue.use(VueDOMPurifyHTML);
Vue.use(VueCroppie);
