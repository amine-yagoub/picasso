import { scryptSync, scrypt, createCipheriv, createDecipheriv, randomFillSync } from 'crypto'
import { Logger } from '@nestjs/common'

export default class AuthHelperService {
  /**
   * @var String
   */
  #request
  /**
   * AuthHelperService constructor.
   *
   * @param configService
   */
  constructor (configService) {
    this.configService = configService
  }

  /**
   *
   * @returns {*}
   */
  get request () {
    return this.#request
  }

  /**
   *
   * @param value
   */
  set request (value) {
    this.#request = value
  }

  /**
   * Encrypt user password
   *
   * @param password
   * @returns {Promise<>}
   */
  async pwdHash (password) {
    return new Promise((resolve, reject) => {
      const salt = this.uuid()
      scrypt(password, salt, 64, (err, derivedKey) => {
        err ? reject(err) : resolve(salt + ':' + derivedKey.toString('hex'))
      })
    })
  }

  /**
   * Verify user password
   *
   * @param password
   * @param hash
   * @returns {Promise<>}
   */
  async pwdVerify (password, hash) {
    return new Promise((resolve, reject) => {
      const [salt, key] = hash.split(':')
      scrypt(password, salt, 64, (err, derivedKey) => {
        err ? reject(err) : resolve(key === derivedKey.toString('hex'))
      })
    })
  }

  /**
   *
   * @param value
   * @returns {Promise<String>}
   */
  encryptNonce (value) {
    return new Promise(resolve => {
      const algorithm = this.configService.get('jwt.nonceEncryptAlg')
      const password = this.configService.get('jwt.nonceEncryptKey')
      const key = scryptSync(password, 'salt', 24)
      const iv = Buffer.alloc(16, 0)
      const cipher = createCipheriv(algorithm, key, iv)
      let encrypted = cipher.update(value, 'utf8', 'hex')
      encrypted += cipher.final('hex')
      resolve(encrypted)
    })
  }

  /**
   *
   * @param value
   * @returns {Promise<String>}
   */
  decryptNonce (value) {
    return new Promise(resolve => {
      const algorithm = this.configService.get('jwt.nonceEncryptAlg')
      const password = this.configService.get('jwt.nonceEncryptKey')
      const key = scryptSync(password, 'salt', 24)
      const iv = Buffer.alloc(16, 0)
      const decipher = createDecipheriv(algorithm, key, iv)
      let decrypted = decipher.update(value, 'hex', 'utf8')
      decrypted += decipher.final('utf8')
      resolve(decrypted)
    })
  }

  /**
   * Generate unique id
   *
   * @returns {*}
   */
  uuid () {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
      (c ^ randomFillSync(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    )
  }
  

  /**
   *
   * @param headers
   * @returns {*}
   */
  getNonce (headers) {
    return headers['user-agent'] + headers['accept-language'] + headers['accept-encoding']
  }

  /**
   *
   * @param nonce
   * @returns {Promise<boolean|boolean>}
   */
  async validateNonce ({ nonce }) {
    const { headers } = this.request
    const nonceName = this.configService.get('jwt.nonceName')
    const regex = new RegExp('(^|;)\\s*' + nonceName + '\\s*=\\s*([^;]+)')
    let isValidNonce = false
    const cookie = headers.cookie
    if(cookie) {
      const nonceEncrypted = regex.exec(cookie)[2] ?? null
      isValidNonce = nonceEncrypted === nonce && await this.decryptNonce(nonce) === this.getNonce(headers)
    }
    if (!isValidNonce) {
      Logger.warn('Invalid Nonce:' + headers.toString())
    }
    return isValidNonce
  }
  
  /**
   * Generate secure password
   *
   * @returns {*}
   */
  generatePassword() {
    const specials = '!@#$%^&*';
    const lowercase = 'abcdefghijklmnopqrstuvwxyz';
    const uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const numbers = '0123456789';
    let password = '';
    password += this.pick(password, specials, 3, 3);
    password += this.pick(password, lowercase, 3, 3);
    password += this.pick(password, uppercase, 3, 3);
    password += this.pick(password, numbers, 3, 5);
    return this.shuffle(password);
  }
  
  /**
   *
   * @param exclusions
   * @param string
   * @param min
   * @param max
   * @returns {string}
   */
  pick(exclusions, string, min, max) {
    let n, chars = '';
    
    if (max === undefined) {
      n = min;
    } else {
      n = min + Math.floor(Math.random() * (max - min + 1));
    }
    
    let i = 0;
    while (i < n) {
      const character = string.charAt(Math.floor(Math.random() * string.length));
      if (exclusions.indexOf(character) < 0 && chars.indexOf(character) < 0) {
        chars += character;
        i++;
      }
    }
    
    return chars;
  }

// Credit to @Christoph: http://stackoverflow.com/a/962890/464744
  shuffle(string) {
    let array = string.split('');
    let tmp, current, top = array.length;
    if (top) while (--top) {
      current = Math.floor(Math.random() * (top + 1));
      tmp = array[current];
      array[current] = array[top];
      array[top] = tmp;
    }
    return array.join('');
  }
}
